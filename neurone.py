import random



# Prend le tableau de x et y
def neurone(tabX):
	refPoids = 10
	refTempo = 0
	for i in range(len(tabX)):
		x = 0
		y = 0
		biais = 0
		# Cette condition est la quand nous avons la bonne solution
		if(refPoids == refTempo):
			# récuperation des données
			x, y = tabX[i].split("_")
			# Génération d'un biais
			biais = gestionBiais(-1, 1)
			refPoids = gestionEntree(float(x), float(y), refPoids)
		else:
			# print result
			return str(refPoids) + "_" + str(x) + "_" + str(biais)
	return 0



def gestionEntree(x, y, refPoids, biais):

	# Random sur une valeur et -20 sa valeur (nous pouvons donc aller dans le negatifs avec cela)
	# on fait sa sur une plage de 20 
	ran = randomPerso(refPoids - 20, refPoids, 0)

	# somme pondérer de notre random et du x passer en parametre du neurone
	somPond = gestionSommePondere(x, ran)
	res = biais + somPond

	#verifivation avec le resultat
	# retourne un poids en fonction du resltat
	if(res == y):
		return refPoids
	elif(res > y):
		return refPoids - 1
	else:
		return refPoids + 1



def gestionBiais(deb, fin):
	# Biais random
	return randomPerso(deb, fin, 0)
	
def gestionSommePondere(x, w):
	# Somme pondéré
	return x * w

def randomPerso(x1, x2, nbApresVirgule):
	return round(random.uniform(x1, x2), nbApresVirgule)
