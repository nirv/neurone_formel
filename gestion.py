import neurone
import matplotlib.pyplot as plt
import math


# fonction d'entre
def init():
	# genere nos données x et y
	don = genererDonneesAffine(7,1)

	ret = neurone.neurone(don)
	# affichage de la courbe de base et la courbe trouver
	if(ret != 0):
		a, x, b = ret.split("_")
		graph(don, a, x, b)


# fonction qui genere les données
def genererDonneesAffine(a, b):
    tab = []
    for x in range(1, 20):
        y = a * x + b
        tab.append(str(x)+"_"+str(y))
    return tab



def graph(don, a1, x1, b1):
	plt.title("Courbes")
	
	xtab = []
	ytab = []
	for i in range(len(don)):
		x, y = don[i].split("_")
		xtab.append(x)	
		ytab.append(y)

	y1 = math.exp(float(x1))
	x2 = float(x1) +1
	y2 = math.exp(x2)

	a1 = plt.subplot(221)
	a1.plot(xtab, ytab)
	a1.set_title("Donneé generer")

	a2 = plt.subplot(222)
	a2.plot([x1, x2], [y1, y2])
	a2.set_title("donnée trouver par le neurone")
	
	plt.show()
















